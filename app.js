require('colors');
const { inquirerMenu, pause, infoTask, listTaskDelete, confirm, showCheckList } = require('./helpers/inquirer');
const { savedFile, readFile } = require('./helpers/savedFile');

const Task = require('./models/task');
const Tasks = require('./models/tasks');
// const { showMenu, pause } = require('./helpers/msg');
console.clear();

const main = async() => {


    let opt = '';
    const newTask = new Tasks();

    const file = readFile();

    if (file) {
        // Establecer las tareas 
        console.log('si existe el file');
        newTask.buildData(file);
    }

    // await pause();

    do {
        // const nT = new Tasks();

        // const t = new Task('Primera descripcion');
        // console.log({ t });

        // nT._listado[t.id] = t;

        // console.log(nT);

        opt = await inquirerMenu();

        switch (opt) {
            case '1':
                const infoT = await infoTask('Descripcion: ');
                // console.log({ infoT });
                newTask.createTask(infoT);
                break;
            case '2':
                // console.log(newTask._listado);
                // console.log(newTask.listarArr);
                newTask.listarTask();
                break;
            case '3':
                newTask.filtarTask(true);
                break;
            case '4':
                newTask.filtarTask(false);

                break;
            case '5':

                const ids = await showCheckList(newTask.listarArr);

                newTask.toggleCompleted(ids);
                // console.log(ids);

                break;

            case '6':
                const idDelete = await listTaskDelete(newTask.listarArr);
                // console.log({ idDelete });

                if (idDelete != '0') {

                    const ok = await confirm('¿Estas seguro de eliminar?');

                    if (ok) {
                        newTask.deleteTask(idDelete);
                        console.log('\n Delete Task');
                    }
                }


                break;
            case '0':

                break;


        }

        savedFile(newTask.listarArr);
        // console.log({ opt });

        if (opt !== '0')
            await pause();

    } while (opt !== '0')
}

main();