const inquirer = require('inquirer');
const colors = require('colors');

const preguntas = [{
    type: 'list',
    name: 'option',
    message: '¿Que desea hacer',
    choices: [{
            value: '1',
            name: `${'1.'.green} ${'Crear Task'.magenta}`
                // name: '1. Crear Task'
        },
        {
            value: '2',
            name: `${'2.'.green} ${'Listar Task'.magenta}`
                // name: '2. Listar Task'
        },
        {
            value: '3',
            name: `${'3.'.green} ${'Listar Task completadas'.magenta}`
        },
        {
            value: '4',
            name: `${'4.'.green} ${'Listar Task incompletas'.magenta}`
        },
        {
            value: '5',
            name: `${'5.'.green} ${'Completar Task (s)'.magenta}`
        },
        {
            value: '6',
            name: `${'6.'.green} ${'Eliminar Task'.magenta}`
        },
        {
            value: '0',
            name: `${'0.'.green} ${'Salir'.magenta}`
        }

    ]
}];

const continuar = [{
    type: 'input',
    name: 'seguir',
    message: `\nPulse ${'ENTER'.green} para continuar\n`
}];

const inquirerMenu = async() => {

    console.clear();

    console.log('======================='.green);
    console.log(colors.bold(' Selecionar una opcion '.blue));
    console.log('=======================\n'.green);

    const { option } = await inquirer.prompt(preguntas);

    return option;

}

const pause = async() => {
    console.log('\n');
    await inquirer.prompt(continuar);
    // return c;
}

const infoTask = async(message) => {
    const infoTask = [{
        type: 'input',
        name: 'info',
        message,
        validate(value) {
            if (value.length === 0) {
                return 'Ingrese una descripcion'
            }
            return true;
        }

    }];

    const { info } = await inquirer.prompt(infoTask);
    return info;
}

const listTaskDelete = async(tasks = []) => {

    const choices = tasks.map((task, i) => {

        const idx = `${i +1}.`.green;
        return {
            value: task.id,
            name: `${idx} ${task.desc}`
        }

    });
    // console.log(choices);

    choices.unshift({
        value: '0',
        name: '0.'.green + ' Cancelar'

    })

    const preguntas = [{
        type: 'list',
        name: 'id',
        message: 'Desea eliminar',
        choices
    }]

    const { id } = await inquirer.prompt(preguntas);
    return id
}

const confirm = async(message) => {
    const questions = [{
        type: 'confirm',
        name: 'ok',
        message
    }]
    const { ok } = await inquirer.prompt(questions);
    return ok
}

const showCheckList = async(tasks = []) => {

    const choices = tasks.map((task, i) => {

        const idx = `${i +1}.`.green;
        return {
            value: task.id,
            name: `${idx} ${task.desc}`,
            checked: (task.completadoEn) ? true : false
        }

    });

    const preguntas = [{
        type: 'checkbox',
        name: 'ids',
        message: 'Selecciones \n',
        choices
    }]

    const { ids } = await inquirer.prompt(preguntas);
    return ids
}

module.exports = {
    inquirerMenu,
    pause,
    infoTask,
    listTaskDelete,
    confirm,
    showCheckList
}