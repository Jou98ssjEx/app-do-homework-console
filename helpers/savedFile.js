const fs = require('fs');
const path = './db/data.json';

const savedFile = (data) => {

    fs.writeFileSync(path, JSON.stringify(data));
}

const readFile = () => {

    if (!fs.existsSync(path)) {
        return null
    }

    const info = fs.readFileSync(path, { encoding: 'utf-8' });
    const data = JSON.parse(info);
    console.log('data', data);

    return data;

}

module.exports = { savedFile, readFile };