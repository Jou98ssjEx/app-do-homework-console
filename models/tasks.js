const Task = require('./task');
const colors = require('colors');
class Tasks {

    get listarArr() {
        const listado = [];

        Object.keys(this._listado).forEach(key => {
            // console.log(key);
            const t = this._listado[key];
            listado.push(t);
        });

        return listado;
    }

    constructor() {
        this._listado = {};
    }

    buildData(data = []) {
        console.log('inicio buildData');

        data.forEach(key => {
            this._listado[key.id] = key
        });
    }

    createTask(desc) {
        const task = new Task(desc);
        this._listado[task.id] = task;
    }

    listarTask() {

        console.log('\n');

        this.listarArr.forEach((tarea, idx) => {

            const i = `${idx + 1}.`.green;
            const { desc, completadoEn } = tarea;
            const c = (completadoEn) ? 'Completada'.green : 'Pendiente'.red

            console.log(`${i} ${desc} :: ${c}`);
        });

        // let completada = 'Completada'.green;
        // let pendiente = 'Pendiente'.red;


        // for (let i = 0; i < this.listarArr.length; i++) {

        //     if (this.listarArr[i].completadoEn != null) {

        //         console.log(`${colors.green( i+1)}. ${this.listarArr[i].desc} :: ${completada}`);
        //     } else {
        //         console.log(`${colors.green( i+1)}. ${this.listarArr[i].desc} :: ${pendiente}`);

        //     }
        // }
    }

    filtarTask(completadas = true) {
        console.log('\n');

        let count = 0;

        this.listarArr.forEach(tarea => {

            // const i = `${idx + 1}.`.green;
            const { desc, completadoEn } = tarea;
            const c = (completadoEn) ? 'Completada'.green : 'Pendiente'.red

            // console.log(`${i} ${desc} :: ${c}`);

            if (completadas === true && c === 'Completada'.green) {
                // console.log('C');
                count += 1;
                console.log(`${count.toString().green} ${desc} :: ${completadoEn.green}`);
            }
            if (completadas === false && c === 'Pendiente'.red) {
                // console.log('C');
                count += 1;
                console.log(`${count.toString().green} ${desc} :: ${c}`);
            }
        });

        // console.log(a);
    }

    deleteTask(id = '') {
        // const data = this.listarArr.find(id);
        // console.log({ data });

        if (this._listado[id]) {
            delete this._listado[id];
        }
    }

    toggleCompleted(ids = []) {
        ids.forEach(id => {
            const task = this._listado[id];

            if (!task.completadoEn) {
                task.completadoEn = new Date().toISOString();
            }
        });

        this.listarArr.forEach(task => {
            if (!ids.includes(task.id)) {
                this._listado[task.id].completadoEn = null;
            }
        })
    }
}

module.exports = Tasks;